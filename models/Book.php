<?php

namespace app\models;

use Yii;
use \yii\db\ActiveRecord;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class Book extends ActiveRecord
{

    public static $bookOffset = 9;

    public static function tableName() {
        return 'book';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['title', 'description', 'author_id'], 'required'],
            [['title', 'description', 'amount', 'author_id', 'page_count', 'cover'], 'safe'],
        ];
    }

    public function fields(){
        return ['id', 'title', 'description', 'amount', 'author_id', 'page_count','cover', 'author', 'category'];
    }

    public function getAuthor(){
        $author = Author::find()->where(['id' => $this->author_id])->one();

        if(empty($author))
            return null;

        return $author->name;
    }

    public function getCategory(){
        $categories = Category::find()
        ->innerJoin(BookCategory::tableName(), BookCategory::tableName().'.category_id = '.Category::tableName().'.id')
        ->innerJoin(Book::tableName(), Book::tableName().'.id = '.BookCategory::tableName().'.book_id')
        ->all();
        $categoryNames = [];
        foreach($categories AS $category){
            $categoryNames[] = $category->name;
        }
        return implode(', ', $categoryNames);
    }

    public function beforeSave($insert){
        return parent::beforeSave($insert);
    }
    
    public function afterSave($insert, $changedAttributes) {
        return parent::afterSave($insert, $changedAttributes);
    }

    public function canOrder(){
        $can = 1;
        $can *= ($this->amount > 0 ? 1 : 0);
        $can *= !$this->isUserOrderingThisBook() ? 1 : 0;
        return $can;
    }

    public function isUserOrderingThisBook(){
        if(empty(\Yii::$app->user->identity))
            return false;

        return Order::find()->where(['user_id' => \Yii::$app->user->identity->id, 'book_id' => $this->id])
        ->andWhere(['<>', 'status', Order::STATUS_RETURNED])->count();
    }

    public static function show($page = 0, $search = false){
        $query = Book::find()
        ->limit(self::$bookOffset)
        ->offset(self::$bookOffset * $page)
        ->orderBy(['id' => SORT_DESC]);

        if($search)
            $query = $query->andWhere(['LIKE', 'title', $search]);
        
        return $query->all();
    }

    public static function removeOne($book_id){
        $book = Book::findOne($book_id);
        $book->amount = $book->amount - 1;
        return $book->save(); 
    }

    public static function addOne($book_id){
        $book = Book::findOne($book_id);
        $book->amount = $book->amount + 1;
        return $book->save();
    }

}
